//
// Created by dimot9 on 1/30/20.
//

#include <set>
#include "cmd.h"
#include "graph.h"

std::vector<std::string> split(std::string const &line, char delimiter) {
    std::vector<std::string> res;
    if (line.empty())
        return res;
    size_t first = 0;
    size_t last;
    while ((last = line.find(delimiter, first)) != std::string::npos) {
        res.push_back(line.substr(first, last-first));
        first = last+1;
    }
    res.push_back(line.substr(first));
    return res;
}


std::pair<int, std::vector<struct Edge>> handle_user_input(std::string input) {
    std::vector<struct Edge> edges;
    std::set<int> vertices; // set to store unique vertices, give us real total number of vertices
    std::vector<std::string> arrayOfValues = split(input, '-');
    for (int i = 0; i < (int) arrayOfValues.size(); i++) {
        std::vector<std::string> values = split(arrayOfValues[i], ',');
        if (values.size() == 3) {

            // Check for the loops in edge definition
            if (values[0] == values[1]) {
                throw std::invalid_argument("Invalid edge definition. Loops are forbidden.");
            }
            try {
                int source = std::stoi(values[0]);
                int destination = std::stoi(values[1]);
                vertices.insert(source);
                vertices.insert(destination);
                int weight = std::stoi(values[2]);
                edges.push_back({source, destination, weight});
            } catch (std::invalid_argument const &e) {
                throw std::invalid_argument("Invalid value. Should be a number.");
            }
        } else {
            throw std::invalid_argument("Invalid number of values. Should be 3: source,destination,weight.");;
        }
    }

    return std::pair<int, std::vector<struct Edge>>(vertices.size(), edges);
}

void print_usage(std::string const& exe_name) {
    std::cout << "\n";
    std::cout << "Usage: " << exe_name << " EDGES [OPTIONS]\n";
    std::cout << "EDGES must be dash (-) delimited three numbers -> source,destination,weight\n";
    std::cout << "\n";
    std::cout << "Commands:\n";
    std::cout << "    -h, --help     Print help\n";
    std::cout << "    -e, --example  Run example of finding MST in a weighted graph\n";
    std::cout << "\n";
    std::cout << "Options:\n";
    std::cout << "    -t, --timer    Print total time needed for finding MST\n";
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

bool is_example(std::string const& argument) {
    return argument == "--example" || argument == "-e";
}

bool is_timer(std::string const& argument) {
    return argument == "--timer" || argument == "-t";
}
