# Borůvkův algoritmus

Minimální kostry mají široké využití - od návrhů tras elektrického vedení, přes vytváření routovacích tabulek v počítačových sítích až po shlukování (clustering) bodů v analýze dat či detekce útvarů v počítačovém vidění.

Minimální kostra se hledá z váženého grafu. Na vstup této aplikace se zadají hrany grafu oddělené pomlčkami. Hrana bude zapsána jako dva vrcholy a její váha (source,destination,weight).
Na výstupu bude seznam hran s jejich vrcholy a váhou, které tvoří minimální kostru a celková váha minimální kostry.

## Build

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Implementace

Má práce sestává ze tří hlavních souborů mimo `main.h` - `graph.h`, `cmd.h` a `app.h`. 

V hlavičkovém souboru `graph.h` definuji třídu Graph - vytváří graph pomocí zadaných hran a 
strukturu Edge, která tyto hrany představuje.

Soubor `cmd.h` definuje veškeré funkce, které se starají o příkazovou řádku a interakci s uživatelem.

Soubor `app.h` definuje funkce, které už vykonávají funkcionalitu Borůvkova algoritmu.

`main.cpp` spouští aplikaci, kontroluje argumenty a odchytává chyby.

`timer.h` obsahuje časovač převzatý z rad v zadání pro semestrální práce https://cw.fel.cvut.cz/wiki/courses/b6b36pjc/ukoly/semestralka.

Při implementaci hledání minimální kostry jsem se inspiroval volně dostupnými pseudokódy na různých portálech - pěkně je popsaný i na Wikipedii: https://cs.wikipedia.org/wiki/Bor%C5%AFvk%C5%AFv_algoritmus

V rámci `cmd.h` se snažím ošetřit nesprávně zadaný vstup od uživatele a vracet mu srozumitelnou odpověď uvádějící, co je v jeho vstupu nejspíš špatně.

Tam kde alokuji paměť tak jí na konci i uvolňuji. V případě tříd a složitějších struktur mám naimplementované destruktory, které paměť na konci bloku uvolní. 
**Valgrind** tedy žádné chyby nehlásí.

## Příklad použití

### Výstup --help
```
Usage: ./blueberry EDGES [OPTIONS]
EDGES must be dash (-) delimited three numbers -> source,destination,weight

Commands:
    -h, --help     Print help
    -e, --example  Run example of finding MST in graph

Options:
    -t, --timer    Print total time needed for finding MST
```

### Konkrétní příklad
```
// Mejme tento graf

            10
        0--------1
        | \      |
       6|  5\    |15
        |     \  |
        2--------3
             4

// vstup
./blueberry 0,1,10-0,2,6-0,3,5-1,3,15-2,3,4 --timer

// vystup
----------CREATED GRAPH----------
Number of vertices: 4
Number of edges: 5
source: 0 - destination: 1 - weight: 10
source: 0 - destination: 2 - weight: 6
source: 0 - destination: 3 - weight: 5
source: 1 - destination: 3 - weight: 15
source: 2 - destination: 3 - weight: 4
----------FINDING MINIMAL SPANNING TREE OF THE GRAPH----------
MST includes: source: 0 - destination: 3 - weight: 5 
MST includes: source: 0 - destination: 1 - weight: 10 
MST includes: source: 2 - destination: 3 - weight: 4 
--------- TOTAL WEIGHT OF MST IS 19 --------
For MST time needed: 51 μs

```

## Měření

Změřený je zatím pouze jednovláknový přístup. 

Naměřeno na tomto stroji:
- RAM: 16GB
- Procesor: Intel® Core™ i7-8750H CPU @ 2.20GHz × 12
- OS: Pop!_OS 19.10

Borůvkův algoritmus má asymptotickou složitost O(E log V), kde V je celkový počet vrcholů ve vstupním grafu.

Vybral jsem dva případy lišící se o 9 vrcholů. Pro každý případ jsem udělal 20 měření a v každém vybral ten nejkratší čas zpracování.

Nejlepší čas naměřený pro 4 vrcholy: 49 mikrosekund

Nejlepší čas naměřený pro 13 vrcholů: 99 mikrosekund

Je zde vidět nějaké zpomalení při vyšším počtu vrcholů, avšak není to žádný obrovský rozdíl, jak by tomu bylo například u exponenciální asymp. složitosti.
Potvrzuje se tím asymp. složitost borůvkova algoritmu O(E log V).
