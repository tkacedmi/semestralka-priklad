//
// Created by dimot9 on 1/29/20.
//

#ifndef BLUEBERRY_ALGORITHM_CMD_H
#define BLUEBERRY_ALGORITHM_CMD_H

#include <ostream>
#include <vector>

using namespace std;

/**
 * Split input string using provided delimiter
 * @param line
 * @param delimiter
 * @return vector of strings, which were delimited by provided delimiter
 */
std::vector<std::string> split(std::string const &line, char delimiter);

/**
 * Parses user input and gets edges for constructing the graph
 * @param input
 * @param V Total number of vertices given
 * @return pair of number of total vertices and vector of edges for the graph
 */
std::pair<int, std::vector<struct Edge>> handle_user_input(std::string input);

/**
 * Prints usage of the app to the user
 * @param exe_name name (path) of the execution file
 */
void print_usage(std::string const& exe_name);

/**
 * Returns true if provided argument by user is help
 * @param argument
 * @return
 */
bool is_help(std::string const& argument);

/**
 * Returns true if provided argument by user is example
 * @param argument
 * @return
 */
bool is_example(std::string const& argument);

/**
 * Returns true if option is timer
 * @param argument
 * @return
 */
bool is_timer(std::string const& argument);


#endif //BLUEBERRY_ALGORITHM_CMD_H
