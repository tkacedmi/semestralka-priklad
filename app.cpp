//
// Created by dimot9 on 1/30/20.
//

#include "app.h"
#include "cmd.h"
#include "graph.h"

Graph* create_graph(const std::string &input) {
    std::pair<int, std::vector<struct Edge>> vertices_edges_pair = handle_user_input(input);
    int V = vertices_edges_pair.first;
    std::vector<struct Edge> edges = vertices_edges_pair.second;
    auto *graph = new Graph(V, edges.size());
    graph->set_edges(edges);

    std::cout << "----------CREATED GRAPH----------\n";
    std::cout << "Number of vertices: " << V << "\n" << "Number of edges: " << edges.size() << "\n";
    graph->print_edges();

    return graph;
}

int find_subset(struct subset subsets[], const int &vertex_index) {
    // find root of the subset
    if (subsets[vertex_index].parent != vertex_index) {
        subsets[vertex_index].parent = find_subset(subsets, subsets[vertex_index].parent);
    }

    return subsets[vertex_index].parent;
}

void union_subsets(struct subset subsets[], const int &set1, const int &set2) {
    int set1_root = find_subset(subsets, set1);
    int set2_root = find_subset(subsets, set2);

    // smaller rank subset (tree) should be under bigger one - union by rank
    if (subsets[set1_root].rank < subsets[set2_root].rank) {
        subsets[set1_root].parent = set2_root;
    } else if (subsets[set1_root].rank > subsets[set2_root].rank) {
        subsets[set2_root].parent = set1_root;
    } else {
        subsets[set2_root].parent = set1_root;
        subsets[set1_root].rank++;
    }
}

void find_mst(const Graph *graph) {

    std::cout << "----------FINDING MINIMAL SPANNING TREE OF THE GRAPH----------\n";

    // allocate array for subsets - maximum number of subsets is total number of vertices (initial subsets)
    auto *subsets = new subset[graph->total_vertices];

    // An array of the cheapest edges
    Edge *cheapest[graph->total_vertices];

    int trees = graph->total_vertices; // number of trees (subtrees) in MST
    int mst_weight = 0; // total weight of minimal spanning tree

    // init total_vertices subsets
    for (int i = 0; i < graph->total_vertices; i++) {
        subsets[i] = {i, 0};
    }

    // while we have more then one trees (sets/subsets)
    while (trees > 1) {
        // init cheapest array after each step
        for (int i = 0; i < graph->total_vertices; i++) {
            cheapest[i] = nullptr;
        }

        for (int i = 0; i < graph->total_edges; i++) {

            // find subsets of each vertices of the edge
            auto set1 = find_subset(subsets, graph->edge[i].source);
            auto set2 = find_subset(subsets, graph->edge[i].destination);

            // if edge is part of the same subset just continue to the next edge
            if (set1 != set2) {

                // if current edge is cheaper then the cheapest edge of the 1st subset so far then replace it
                if (cheapest[set1] == nullptr || graph->edge[i].weight < cheapest[set1]->weight) {
                    cheapest[set1] = &graph->edge[i];
                }

                // if current edge is cheaper then the cheapest edge of the 2nd subset so far then replace it
                if (cheapest[set2] == nullptr || graph->edge[i].weight < cheapest[set2]->weight) {
                    cheapest[set2] = &graph->edge[i];
                }
            }
        }

        // add cheapest edges to mst and union subsets
        for (int v = 0; v < graph->total_vertices; ++v) {

            if (cheapest[v] != nullptr) {
                auto set1 = find_subset(subsets, cheapest[v]->source);
                auto set2 = find_subset(subsets, cheapest[v]->destination);

                if (set1 != set2) {
                    mst_weight += cheapest[v]->weight;
                    printf("MST includes: source: %d - destination: %d - weight: %d \n", cheapest[v]->source, cheapest[v]->destination, cheapest[v]->weight);
                    union_subsets(subsets, set1, set2);
                    --trees;
                }
            }
        }
    }

    printf("--------- TOTAL WEIGHT OF MST IS %d --------\n", mst_weight);

    // free allocate memory for array pointer initialized above
    delete[] subsets;
}

void run_example() {
    std::cout << "\n";
    std::cout << "Running example with following arguments: " << EXAMPLE_INPUT_EDGES << "\n";
    std::cout << "-----------------------------------------------------------------------------\n";

    Graph *graph = create_graph(EXAMPLE_INPUT_EDGES);
    find_mst(graph);
    delete graph;
}
