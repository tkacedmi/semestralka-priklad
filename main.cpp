//
// Created by dimot9 on 1/26/20.
//

#include <iostream>
#include <algorithm>
#include "timer.h"
#include "app.h"
#include "cmd.h"

int main(int argc, char** argv)
{

    if (std::any_of(argv, argv+argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    } else if (std::any_of(argv, argv+argc, is_example)) {
        try {
            run_example();
            return 0;
        } catch (const std::exception &e) {
            std::clog << e.what() << "\n" << " Type --help or -h for more details.\n";
            return 1;
        }
    }

    bool use_timer = false;

    if (argc < 2 || argc > 3) {
        std::clog << "Bad or no arguments given.\n";
        print_usage(argv[0]);
        return 1;
    } else if (argc > 2) {
        if (!is_timer(argv[2])) {
            std::clog << "Unknown option given.\n";
            print_usage(argv[0]);
            return 1;
        } else {
            use_timer = true;
        }
    }

    try {
        Graph *graph = create_graph(argv[1]);
        auto start_time = timer::now();
        find_mst(graph);
        delete graph;
        auto end_time = timer::now();
        if (use_timer) {
            printf("For MST time needed: %lld μs\n", timer::to_ms(end_time - start_time));
        }
    } catch (const std::exception &e) {
        std::clog << e.what() << "\n" << " Type --help or -h for more details.\n";
        return 1;
    }

    return 0;
}
