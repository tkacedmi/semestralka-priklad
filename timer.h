//
// Created by dimot9 on 1/31/20.
//

#ifndef BLUEBERRY_ALGORITHM_TIMER_H
#define BLUEBERRY_ALGORITHM_TIMER_H

#include <chrono>

// source: https://cw.fel.cvut.cz/wiki/courses/b6b36pjc/ukoly/semestralka
namespace timer {

    /**
     * Gets now time
     */
    auto now() {
        return std::chrono::high_resolution_clock::now();
    }

    /**
     * Converts given time to milliseconds
     * @tparam TimePoint
     * @param tp
     * @return
     */
    template <typename TimePoint>
    long long to_ms(TimePoint tp) {
        return std::chrono::duration_cast<std::chrono::microseconds>(tp).count();
    }

}

#endif //BLUEBERRY_ALGORITHM_TIMER_H
