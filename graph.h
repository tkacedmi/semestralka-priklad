//
// Created by dimot9 on 1/30/20.
//

#ifndef BLUEBERRY_ALGORITHM_GRAPH_H
#define BLUEBERRY_ALGORITHM_GRAPH_H

#include <vector>
#include <iostream>

using namespace std;

/**
 * Represents edge of the graph from two vertices (source, destination) and weight of the edge.
 */
struct Edge {
    int source, destination, weight;

    ~Edge() = default;
};

/**
 * The graph defined by user. It holds vertices and edges.
 */
class Graph {

public:
    int total_vertices, total_edges;

    /**
    * Array of edges
    */
    Edge* edge;

    /**
     * Initializes empty graph with given number of vertices and edges.
     * @param V Sum of vertices
     * @param E Sum of edges
     */
    Graph(const int &V, const int &E);

    /**
     * Destructor - frees allocated memory
     */
    ~Graph() {
        delete[] edge;
        edge = nullptr;
    }

    /**
     * Fills the graph with edges from an array of edges.
     * @param edge
     */
    void set_edges(Edge *edge);

    /**
     * Fills the graph with edges from a vector of edges.
     */
    void set_edges(std::vector<struct Edge>);

    /**
     * Prints edges of the graph.
     */
    void print_edges() {
        for (int i = 0; i < total_edges; i++) {
            std::cout << "source: " << this->edge[i].source << " - destination: " << this->edge[i].destination << " - weight: " << this->edge[i].weight << "\n";
        }
    }
};

#endif //BLUEBERRY_ALGORITHM_GRAPH_H
