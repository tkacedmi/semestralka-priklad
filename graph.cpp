//
// Created by dimot9 on 1/30/20.
//

#include "graph.h"

Graph::Graph(const int &V, const int &E): total_vertices(V), total_edges(E) {
    this->edge = new Edge[E];
}

void Graph::set_edges(struct Edge edges[]) {
    for (int i = 0; i < total_edges; i++) {
        this->edge[i] = edges[i];
    }
}

void Graph::set_edges(std::vector<struct Edge> edges) {
    for (int i = 0; i < total_edges; i++) {
        this->edge[i] = edges[i];
    }
}
