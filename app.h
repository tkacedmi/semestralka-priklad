//
// Created by dimot9 on 1/30/20.
//

#include <string>
#include "graph.h"

#ifndef BLUEBERRY_ALGORITHM_APP_H
#define BLUEBERRY_ALGORITHM_APP_H


static const std::string EXAMPLE_INPUT_EDGES = "0,1,10-0,2,6-0,3,5-1,3,15-2,3,4";

/**
 * Runs an example of the app
 */
void run_example();

/**
 * Represents subset of the tree
 */
struct subset {
    int parent;
    int rank;

    ~subset() = default;
};

/**
 * Gets edges defined by user and creates a graph from these edges
 * @param input User's graph definition using edges
 */
Graph* create_graph(const std::string &input);

/**
 * Finds subset of the given vertex (root vertex of the subset)
 * @param vertex_index
 * @return index of the subset
 */
int find_subset(struct subset subsets[], const int &vertex_index);

/**
 * Union two given subsets
 * @param set1
 * @param set2
 */
void union_subsets(struct subset subsets[], const int &set1, const int &set2);

/**
 * Creates minimal spanning tree from the given graph
 * @param graph
 */
void find_mst(const Graph *graph);

#endif //BLUEBERRY_ALGORITHM_APP_H
